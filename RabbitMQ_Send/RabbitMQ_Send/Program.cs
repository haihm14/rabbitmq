﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ_Send
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] ms = new String[2];
            ms[0] = "Mesage send to Red";
            ms[1] = "Mesage send to Blue";

            var factory = new ConnectionFactory() { HostName = "localhost",  };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                //channel.QueueDeclare(queue: "task_queue",
                //                     durable: false,
                //                     exclusive: false,
                //                     autoDelete: false,
                //                     arguments: null);

                var message = GetMessage(args);
                var body = Encoding.UTF8.GetBytes(ms[1]);

                //var properties = channel.CreateBasicProperties();
                //properties.Persistent = true;

                // channel.ExchangeDeclare("logsDirect", ExchangeType.Direct);


                // send to consumerBlue
                channel.BasicPublish(exchange: "request.exchange",
                                     routingKey: "directexchange_key_blue",
                                     basicProperties: null,
                                     body: body);
                Console.WriteLine(" [x] Sent {0}", message);


                body = Encoding.UTF8.GetBytes(ms[0]);
                // send to consumerRed
                channel.BasicPublish(exchange: "request.exchange",
                                    routingKey: "directexchange_key_red",
                                    basicProperties: null,
                                    body: body);
                Console.WriteLine(" [x] Sent {0}", message);
            }

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }

        private static string GetMessage(string[] args)
        {
            return ((args.Length > 0) ? string.Join(" ", args) : "Hello World!");
        }
    }
}
